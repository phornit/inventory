<?php


namespace App\Libraries;


use App\Http\Controllers\Controller;
use App\Models\Auth\UserInformation;
use App\Models\Auth\UserRole;
use App\User;

class UtilityFunction extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    static function getCurrentUser(){
        $data = User::find(auth()->user()->id);
        return $data;
    }

    static function getCurrentUserRole(){
        $currentUserId = User::find(auth()->user()->id);
        $currentUserRole = UserRole::where('sec_user_id',$currentUserId->id)->pluck('role_id');
        return $currentUserRole;
    }

    static function getCurrentUserInfo(){
        $data = UserInformation::find(auth()->user()->id);
        return $data;
    }

    static function currentCompanyId(){
        return self::getCurrentUserInfo()['company_id'];
    }

    static function currentBranchId(){
        return self::getCurrentUserInfo()['branch_id'];
    }

    static function currentLanguage(){
        return self::getCurrentUser()['default_language'];
    }

}
