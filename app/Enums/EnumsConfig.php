<?php

namespace App\Enums;

use BenSampo\Enum\Enum;

/**
 * BNPumList = Backend pagination number list
 * BNPumList = Backend pagination number grid
 */
final class EnumsConfig extends Enum
{
    const BPNumList =   30;
    const BPNumGrid =   20;
    const XAPIKEY = '1ccbc4c913bc4ce785a0a2de444aa0d6';
}
