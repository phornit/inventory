<?php

namespace App\Models\Menus;

use Illuminate\Database\Eloquent\Model;

class BackendMenu extends Model
{
    protected $table = "core_backend_menu";
    protected $fillable = [
        'parent_id',
        'title_kh',
        'title_en',
        'link',
        'class',
        'is_active',
        'color',
        'ordering'
    ];
}
