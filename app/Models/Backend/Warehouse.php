<?php

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class Warehouse extends Model
{
    protected $table = "warehouse";
    protected $fillable = [
        'code',
        'name',
        'description',
        'company_id',
        'is_active'
    ];
}
