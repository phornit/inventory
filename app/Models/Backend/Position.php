<?php

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class Position extends Model
{
    protected $table = 'p_position';
    protected $fillable = [
        'id',
        'company_id',
        'title_kh',
        'title_en',
        'description',
        'is_active',
    ];
}
