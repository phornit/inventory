<?php

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class Branch extends Model
{
    protected $table = 'branch';
    protected $fillable = [
        'company_id',
        'city_id',
        'currency_id',
        'branch_code',
        'branch_name',
        'branch_phone',
        'branch_address',
        'is_active',
        'is_delete'
    ];

    static function getBranchName($id){
        $data = Branch::where('id', $id)->pluck('branch_name')->first();
        return $data;
    }
}
