<?php

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    protected $table = 'currency';
    protected $fillable = [
        'name',
        'abbr',
        'symbol',
        'description',
        'is_active'
    ];

    static function getCurrencyName($id){
        $data = Currency::where('id', $id)->pluck('name')->first();
        return $data;
    }

    static function getSymbol($id){
        $data = Currency::where('id', $id)->pluck('symbol')->first();
        return $data;
    }
}
