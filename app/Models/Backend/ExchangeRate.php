<?php

namespace App\Models\Backend;

use App\Models\Auth\Role;
use Illuminate\Database\Eloquent\Model;

class ExchangeRate extends Model
{
    protected $table = "exchange_rate";
    protected $fillable = [
        'company_id',
        'from_currency_id',
        'to_currency_id',
        'exchange_rate',
        'description',
        'is_active'
    ];

    static function getExchangeRateBranch($exchange_rate_id){
        $exchange_rate = ExchangeRateBranch::where('exchange_rate_id',$exchange_rate_id)->get();
        $data = $exchange_rate->map(function($page){
            return [
                'id' => $page->branch_id,
                'branch_name' => Branch::where('id', $page->branch_id)->pluck('branch_name')->first(),
            ];
        });
        return $data;
    }
}
