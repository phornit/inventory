<?php

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class CategoryBranch extends Model
{
    protected $table = 'category_branch';
    public $timestamps = false;
    protected $fillable = [
        'category_id',
        'branch_id'
    ];
}
