<?php

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    protected $table = 'p_group';
    protected $fillable = [
        'id',
        'company_id',
        'title_kh',
        'title_en',
        'description',
        'is_active',
    ];
}
