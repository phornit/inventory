<?php

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'category';
    protected $fillable = [
        'company_id',
        'parent_id',
        'name',
        'description',
        'thumbnail',
        'is_active'
    ];

    static function getCategoryBranch($category_id){
        $categoryBranch = CategoryBranch::where('category_id', $category_id)->get();
        $data = $categoryBranch->map(function($page){
            return [
                'id' => $page->branch_id,
                'branch_name' => Branch::where('id', $page->branch_id)->pluck('branch_name')->first()
            ];
        });
        return $data;
    }

    static function getSubCategory($category_id){
        $category = Category::where('parent_id', $category_id)->get();
        $data = $category->map(function($page){
            return [
                'id' => $page->id,
                'company_id' => $page->company_id,
                'parent_id' => $page->parent_id,
                'name' => $page->name,
                'description' => $page->description,
                'thumbnail' => $page->thumbnail,
                'is_active' => $page->is_active,
                'branches' => Category::getCategoryBranch($page->id),
            ];
        });
        return $data;
    }
}
