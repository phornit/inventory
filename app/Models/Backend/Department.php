<?php

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class Department extends Model
{
    protected $table = 'p_department';
    protected $fillable = [
        'id',
        'company_id',
        'title_kh',
        'title_en',
        'description',
        'is_active',
    ];
}
