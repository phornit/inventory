<?php

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class ExchangeRateBranch extends Model
{
    protected $table = "exchange_rate_branch";
    public $timestamps = false;
    protected $fillable = [
        'branch_id',
        'exchange_rate_id'
    ];
}
