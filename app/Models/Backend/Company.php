<?php

namespace App\Models\Backend;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
    protected $table = "company";
    protected $fillable = [
        'name_kh',
        'name_en',
        'sub_name',
        'phone',
        'email',
        'facebook',
        'website',
        'address',
        'city_id',
        'logo'
    ];
}
