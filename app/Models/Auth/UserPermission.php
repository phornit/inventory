<?php

namespace App\Models\Auth;

use Illuminate\Database\Eloquent\Model;

class UserPermission extends Model
{
    protected $table = "core_user_permission";
    protected $fillable = [
        'role_id',
        'backend_menu_id',
        'm_view',
        'm_insert',
        'm_update',
        'm_delete'
    ];
}
