<?php

namespace App\Models\Auth;

use Illuminate\Database\Eloquent\Model;

class UserInformation extends Model
{
    protected $table = "core_user_information";
    protected $fillable = [
        'name_kh',
        'name_en',
        'gender',
        'date_of_birth',
        'place_of_birth',
        'phone',
        'address',
        'city_id',
        'country_id',
        'nationality_id',
        'position_id',
        'group_id',
        'department_id',
        'skill',
        'degree',
        'photo',
        'sec_user_id',
        'company_id',
        'branch_id'
    ];
}
