<?php

namespace App\Models\Auth;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $table = "core_role";
    protected $fillable = [
        'name',
        'description',
        'is_active',
    ];
}
