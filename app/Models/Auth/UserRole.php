<?php

namespace App\Models\Auth;

use Illuminate\Database\Eloquent\Model;

class UserRole extends Model
{
    protected $table = "core_sec_user_role";
    public $timestamps = false;
    protected $fillable = [
        'sec_user_id',
        "role_id"
    ];
}
