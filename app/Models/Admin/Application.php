<?php

namespace App\Models\Admin;

use Illuminate\Database\Eloquent\Model;

class Application extends Model
{
    protected $table = "core_application";
    protected $fillable = [
        'parent_id',
        'title_kh',
        'title_en',
        'link',
        'class',
        'is_active',
        'color',
        'description',
        'ordering'
    ];
}
