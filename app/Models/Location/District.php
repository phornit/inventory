<?php

namespace App\Models\Location;

use Illuminate\Database\Eloquent\Model;

class District extends Model
{
    protected $table = "loc_district";
    public $timestamps = false;
    protected $fillable = [
        'city_id',
        'post_code',
        'title_kh',
        'title_en',
        'is_active'
    ];

    static function getDistrictByCommuneId($CommuneId){
        $commune = Commune::find($CommuneId);
        $disctrict = District::where('id', $commune->district_id)->first();
        return $disctrict;
    }
}
