<?php

namespace App\Models\Location;

use Illuminate\Database\Eloquent\Model;

class Commune extends Model
{
    protected $table = "loc_commune";
    public $timestamps = false;
    protected $fillable = [
        'district_id',
        'post_code',
        'title_kh',
        'title_en',
        'is_active'
    ];
}
