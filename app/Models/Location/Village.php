<?php

namespace App\Models\Location;

use Illuminate\Database\Eloquent\Model;

class Village extends Model
{
    protected $table = "loc_village";
    public $timestamps = false;
    protected $fillable = [
        'commune_id',
        'post_code',
        'title_kh',
        'title_en',
        'is_active'
    ];
}
