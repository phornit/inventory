<?php

namespace App\Models\Location;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    protected $table = "loc_country";
    public $timestamps = false;
    protected $fillable = [
        'post_code',
        'alpha_2_code',
        'alpha_3_code',
        'title',
        'nationality',
        'is_active'
    ];

    static function getCountryById($id){
        $country = Country::find($id);
        return $country;
    }
}
