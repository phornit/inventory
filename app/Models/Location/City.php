<?php

namespace App\Models\Location;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $table = "loc_city";
    public $timestamps = false;
    protected $fillable = [
        'post_code',
        'title_kh',
        'title_en',
        'is_active'
    ];

    static function getCityById($cityId){
        $city = City::find($cityId);
        return $city;
    }

    static function getCityByCommuneId($CommuneId){
        $district = District::getDistrictByCommuneId($CommuneId);
        $city = City::where('id', $district->city_id)->first();
        return $city;
    }

    static function getCityByDistrictId($districtId){
        $district = District::find($districtId);
        $city = City::where('id', $district->city_id)->first();
        return $city;
    }
}
