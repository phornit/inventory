<?php

namespace App\Http\Resources;

use App\Models\Auth\Role;
use App\Models\Auth\UserRole;
use Illuminate\Http\Resources\Json\JsonResource;

class SecUserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'password' => $this->password,
            'is_active' => $this->is_active,
            'user_role' => $this->user_role($this->id)
        ];
    }

    public function user_role($sec_user_id){
        $userRole = UserRole::where('sec_user_id', $sec_user_id)->get();

        $data = $userRole->map(function($page){
            return [
                'id' => $page->role_id,
                'name' => Role::where('id', $page->role_id)->pluck('name')->first(),
            ];
        });

        return $data;
    }

    public function with($request){
        return [
            'statusCode' => 200,
            'message' => 'Success'
        ];
    }
}
