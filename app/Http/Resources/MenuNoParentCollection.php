<?php

namespace App\Http\Resources;

use App\Models\Backend\BackendMenu;
use Illuminate\Http\Resources\Json\ResourceCollection;

class MenuNoParentCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => $this->collection->map(function($page){
                return [
                    'id' => 0,
                    'backend_menu_id' => $page->id,
                    'title_en' => $page->title_en,
                    'title_kh' => $page->title_kh,
                    'm_view' => false,
                    'm_insert' => false,
                    'm_update' => false,
                    'm_delete' => false,
                ];
            }),
        ];
    }

    public function with($request){
        return [
            'statusCode' => 200,
            'message' => 'Success'
        ];
    }
}
