<?php

namespace App\Http\Resources;

use App\Models\Location\City;
use App\Models\Location\Commune;
use App\Models\Location\District;
use Illuminate\Http\Resources\Json\JsonResource;

class VillageResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'commune_id' => $this->commune_id,
            'commune_name' => Commune::where('id',$this->commune_id)->pluck('title_kh')->first(),
            'district_id' => District::getDistrictByCommuneId($this->commune_id)->id,
            'district_name' => District::getDistrictByCommuneId($this->commune_id)->title_kh,
            'city_id' => City::getCityByCommuneId($this->commune_id)->id,
            'city_name' => City::getCityByCommuneId($this->commune_id)->title_kh,
            'post_code' => $this->post_code,
            'title_kh' => $this->title_kh,
            'title_en' => $this->title_en,
            'is_active' => $this->is_active,
        ];
    }

    public function with($request){
        return [
            'statusCode' => 200,
            'message' => 'Success'
        ];
    }
}
