<?php

namespace App\Http\Resources;

use App\Models\Location\City;
use App\Models\Location\Commune;
use App\Models\Location\District;
use Illuminate\Http\Resources\Json\ResourceCollection;

class VillageCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => $this->collection->map(function($page){
                return [
                    'id' => $page->id,
                    'commune_id' => $page->commune_id,
                    'commune_name' => Commune::where('id',$page->commune_id)->pluck('title_kh')->first(),
                    'district_id' => District::getDistrictByCommuneId($page->commune_id)->id,
                    'district_name' => District::getDistrictByCommuneId($page->commune_id)->title_kh,
                    'city_id' => City::getCityByCommuneId($page->commune_id)->id,
                    'city_name' => City::getCityByCommuneId($page->commune_id)->title_kh,
                    'post_code' => $page->post_code,
                    'title_kh' => $page->title_kh,
                    'title_en' => $page->title_en,
                    'is_active' => $page->is_active,
                ];
            }),
        ];
    }

    public function with($request){
        return [
            'statusCode' => 200,
            'message' => 'Success'
        ];
    }
}
