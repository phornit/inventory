<?php

namespace App\Http\Resources;

use App\Models\Auth\UserInformation;
use Illuminate\Http\Resources\Json\JsonResource;

class currentUserResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'user_id' => self::UserInfo($this->id)['id'],
            'name' => self::UserInfo($this->id)['name_en'],
            'email' => $this->email,
            'sec_user_id' => $this->id,
            'company_id' => self::UserInfo($this->id)['company_id'],
            'branch_id' => self::UserInfo($this->id)['branch_id'],
        ];
    }

    static function UserInfo($secUserId){
        $data = UserInformation::where('sec_user_id', $secUserId)->first();
        return $data;
    }

    public function with($request){
        return [
            'statusCode' => 200,
            'message' => 'Success'
        ];
    }
}
