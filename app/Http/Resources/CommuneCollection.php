<?php

namespace App\Http\Resources;

use App\Models\Location\City;
use App\Models\Location\District;
use Illuminate\Http\Resources\Json\ResourceCollection;

class CommuneCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => $this->collection->map(function($page){
                return [
                    'id' => $page->id,
                    'district_id' => $page->district_id,
                    'district_name' => District::where('id',$page->district_id)->pluck('title_kh')->first(),
                    'city_id' => City::getCityByDistrictId($page->district_id)->id,
                    'city_name' => City::getCityByDistrictId($page->district_id)->title_kh,
                    'post_code' => $page->post_code,
                    'title_kh' => $page->title_kh,
                    'title_en' => $page->title_en,
                    'is_active' => $page->is_active,
                ];
            }),
        ];
    }

    public function with($request){
        return [
            'statusCode' => 200,
            'message' => 'Success'
        ];
    }
}
