<?php

namespace App\Http\Resources;

use App\Models\Backend\Branch;
use App\Models\Backend\Currency;
use App\Models\Backend\ExchangeRate;
use Illuminate\Http\Resources\Json\ResourceCollection;

class ExchangeRateCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => $this->collection->map(function($page){
                return [
                    'id' => $page->id,
                    'company_id' => $page->company_id,
                    'from_currency_id' => $page->from_currency_id,
                    'from_currency_name' => Currency::getCurrencyName($page->from_currency_id),
                    'from_currency_symbol' => Currency::getSymbol($page->from_currency_id),
                    'to_currency_id' => $page->to_currency_id,
                    'to_currency_name' => Currency::getCurrencyName($page->to_currency_id),
                    'to_currency_symbol' => Currency::getSymbol($page->to_currency_id),
                    'exchange_rate' => $page->exchange_rate,
                    'description' => $page->description,
                    'is_active' => $page->is_active,
                    'branches' => ExchangeRate::getExchangeRateBranch($page->id),
                ];
            }),
        ];
    }

    public function with($request){
        return [
            'statusCode' => 200,
            'message' => 'Success'
        ];
    }
}
