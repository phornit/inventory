<?php

namespace App\Http\Resources;

use App\Models\Location\City;
use App\Models\Location\Country;
use Illuminate\Http\Resources\Json\JsonResource;
use phpDocumentor\Reflection\Types\This;

class CompaniesResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name_kh' => $this->name_kh,
            'name_en' => $this->name_en,
            'sub_name' => $this->sub_name,
            'phone' => $this->phone,
            'email' => $this->email,
            'facebook' => $this->facebook,
            'website' => $this->website,
            'address' => $this->address,
            'city_id' => $this->city_id,
            'city_name_kh' => City::getCityById($this->city_id)->title_kh,
            'city_name_en' => City::getCityById($this->city_id)->title_en,
            'country_id' => $this->country_id,
            'country_title' => Country::getCountryById($this->country_id)->title,
            'logo' => $this->logo,
            'is_active' => $this->is_active
        ];
    }

    public function with($request){
        return [
            'statusCode' => 200,
            'message' => 'Success'
        ];
    }
}
