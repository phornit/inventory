<?php

namespace App\Http\Resources;

use App\Libraries\UtilityFunction;
use App\Models\Auth\UserPermission;
use App\Models\Menus\BackendMenu;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Facades\DB;

class BackendMenuCollection extends ResourceCollection
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => $this->collection->map(function($page){
                return [
                    'id' => $page->id,
                    'parent_id' => $page->parent_id,
                    'title_kh' => $page->title_kh,
                    'title_en' => $page->title_en,
                    'link' => $page->link,
                    'class' => $page->class,
                    'is_active' => $page->is_active,
                    'color' => $page->color,
                    'ordering' => $page->ordering,
                    'm_view' => self::viewPermission($page->link),
                    'subMenu' => self::SubBackendMenu($page->id)
                ];
            }),
        ];
    }

    static function viewPermission($menu_link){
        $currentRoleId = UtilityFunction::getCurrentUserRole();
        $menu = BackendMenu::where('link', $menu_link)->first();
        $data = UserPermission::whereIn('role_id', $currentRoleId)->where('backend_menu_id', $menu->id)->distinct()->first();
        return $data['m_view'];
    }

    static function SubBackendMenu($menu_id){
        $currentRoleId = UtilityFunction::getCurrentUserRole();
        $data = DB::table('core_backend_menu')
            ->join('core_user_permission','core_backend_menu.id','core_user_permission.backend_menu_id')
            ->where('core_backend_menu.parent_id',$menu_id)
            ->whereIn('core_user_permission.role_id', $currentRoleId)
            ->select('core_backend_menu.*', 'core_user_permission.m_view')
            ->orderBy('ordering','desc')
            ->distinct()
            ->get();
        return $data;

    }

    public function with($request){
        return [
            'statusCode' => 200,
            'message' => 'Success'
        ];
    }
}
