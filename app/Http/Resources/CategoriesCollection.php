<?php

namespace App\Http\Resources;

use App\Models\Backend\Branch;
use App\Models\Backend\Category;
use App\Models\Backend\CategoryBranch;
use Illuminate\Http\Resources\Json\ResourceCollection;

class CategoriesCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => $this->collection->map(function($page){
                return [
                    'id' => $page->id,
                    'company_id' => $page->company_id,
                    'parent_id' => $page->parent_id,
                    'name' => $page->name,
                    'description' => $page->description,
                    'thumbnail' => $page->thumbnail,
                    'is_active' => $page->is_active,
                    'branches' => Category::getCategoryBranch($page->id),
                    'subCategory' => Category::getSubCategory($page->id)
                ];
            }),
        ];
    }

    public function with($request){
        return [
            'statusCode' => 200,
            'message' => 'Success'
        ];
    }
}
