<?php

namespace App\Http\Resources;

use App\Models\Location\City;
use App\Models\Location\District;
use Illuminate\Http\Resources\Json\JsonResource;

class CommuneResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'district_id' => $this->district_id,
            'district_name' => District::where('id',$this->district_id)->pluck('title_kh')->first(),
            'city_id' => City::getCityByDistrictId($this->district_id)->id,
            'city_name' => City::getCityByDistrictId($this->district_id)->title_kh,
            'post_code' => $this->post_code,
            'title_kh' => $this->title_kh,
            'title_en' => $this->title_en,
            'is_active' => $this->is_active,
        ];
    }

    public function with($request){
        return [
            'statusCode' => 200,
            'message' => 'Success'
        ];
    }
}
