<?php

namespace App\Http\Resources;

use App\Models\Backend\Currency;
use App\Models\Backend\ExchangeRate;
use Illuminate\Http\Resources\Json\JsonResource;

class ExchangeRateResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'company_id' => $this->company_id,
            'from_currency_id' => $this->from_currency_id,
            'to_currency_id' => $this->to_currency_id,
            'exchange_rate' => $this->exchange_rate,
            'description' => $this->description,
            'is_active' => $this->is_active,
            'branches' => ExchangeRate::getExchangeRateBranch($this->id),
        ];
    }

    public function with($request){
        return [
            'statusCode' => 200,
            'message' => 'Success'
        ];
    }
}
