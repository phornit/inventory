<?php

namespace App\Http\Resources;

use App\Models\Location\City;
use Illuminate\Http\Resources\Json\ResourceCollection;

class DistrictCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => $this->collection->map(function($page){
                return [
                    'id' => $page->id,
                    'city_id' => $page->city_id,
                    'city_name' => City::where('id',$page->city_id)->pluck('title_kh')->first(),
                    'post_code' => $page->post_code,
                    'title_kh' => $page->title_kh,
                    'title_en' => $page->title_en,
                    'is_active' => $page->is_active,
                ];
            }),
        ];
    }

    public function with($request){
        return [
            'statusCode' => 200,
            'message' => 'Success'
        ];
    }
}
