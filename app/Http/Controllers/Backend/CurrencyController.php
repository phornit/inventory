<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Libraries\CoreFunction;
use App\Models\Backend\Currency;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CurrencyController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $num = CoreFunction::Pagination();
        $data = Currency::orderBy('id','desc')->paginate($num);
        return $this->sendListResponse($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'symbol' => ['required', 'string', 'max:255'],
        ]);

        if($validator->fails()){
            return $this->sendInvalidResponse($validator->errors());
        }

        $request['is_active'] = !($request['is_active']) ? 0 : 1;

        $data = Currency::create($request->all());
        return $this->sendResponse($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Currency::find($id);
        return $this->sendResponse($data);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
            'symbol' => ['required', 'string', 'max:255'],
        ]);

        if($validator->fails()){
            return $this->sendInvalidResponse($validator->errors());
        }

        $data = Currency::find($id);
        $data->update($request->all());

        return $this->sendResponse($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $arr_id = explode(',', $id);
        $data = "";
        foreach ($arr_id as $val) {
            $data = Currency::find($val);
            $data -> delete();
        }
        return $this->sendResponse($data);
    }

    public function isActive(Request $request ,$id){
        $arr_id = explode(',', $id);
        $data = "";
        foreach ($arr_id as $val) {
            $data = Currency::find($val);
            $data ->update([
                'is_active' => $request['is_active']
            ]);
        }
        return $this->sendResponse($data);
    }
}
