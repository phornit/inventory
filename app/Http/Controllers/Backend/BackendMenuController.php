<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Resources\BackendMenuCollection;
use App\Http\Resources\MenuNoParentCollection;
use App\Libraries\CoreFunction;
use App\Libraries\UtilityFunction;
use App\Models\Menus\BackendMenu;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class BackendMenuController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $num = CoreFunction::Pagination();
        $data = BackendMenu::where('parent_id',0)->orderBy('ordering', 'desc')->paginate($num);

        return new BackendMenuCollection($data);
    }

    public function getAll()
    {
        $data = BackendMenu::where('is_active',1)->orderBy('ordering', 'desc')->get();
        return $this->sendListResponse($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title_en' => ['required', 'string', 'max:255'],
            'title_kh' => ['required', 'string', 'max:255']
        ]);

        $request['is_active'] = (!empty($request['is_active'])) ? 1 : 0;

        if($validator->fails()){
            return $this->sendInvalidResponse($validator->errors());
        }

        $data = BackendMenu::create($request->all());
        return $this->sendResponse($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = BackendMenu::find($id);
        return $this->sendResponse($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'title_kh' => ['required', 'string', 'max:255'],
            'title_en' => ['required', 'string', 'max:255']
        ]);

        if($validator->fails()){
            return $this->sendInvalidResponse($validator->errors());
        }

        $request['is_active'] = (!empty($request['is_active'])) ? 1 : 0;

        $data = BackendMenu::find($id);
        $data->update($request->all());
        return $this->sendResponse($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = BackendMenu::find($id);
        $data->delete();
        return $this->sendResponse($data);
    }

    public function updateOrdering(Request $request){
        $total = $request['total'];
        $menus = $request['menus'];
        foreach ($menus as $menu){
            $id = $menu['id'];
            $backendMenu = BackendMenu::find($id);
            $backendMenu->update([
                'ordering' => $total
            ]);
            $total = $total-1 ;
        }
        return $this->sendResponse($menus);
    }

    public function getAllParent()
    {
        $data = BackendMenu::where('parent_id',0)->where('is_active',1)->orderBy('ordering', 'desc')->get();
        return new BackendMenuCollection($data);
    }

    public function getNoParent(){
        $menu = BackendMenu::groupBy('parent_id')->where('parent_id','>', 0)->get('parent_id');
        $data = BackendMenu::whereNotIn('id', $menu)->get();
        return new MenuNoParentCollection($data);
    }

    public function getAllMenus(){
        $data = BackendMenu::all();
        return new MenuNoParentCollection($data);
    }

    public function isActive(Request $request ,$id){
        $arr_id = explode(',', $id);
        $data = "";
        foreach ($arr_id as $val) {
            $data = BackendMenu::find($val);
            $data ->update([
                'is_active' => $request['is_active']
            ]);
        }
        return $this->sendResponse($data);
    }

    public function remove($id){
        $arr_id = explode(',', $id);
        $data = "";
        foreach ($arr_id as $val) {
            $data = BackendMenu::find($val);
            $data -> delete();
        }
        return $this->sendResponse($data);
    }
}
