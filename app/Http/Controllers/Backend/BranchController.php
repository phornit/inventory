<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Libraries\CoreFunction;
use App\Libraries\UtilityFunction;
use App\Models\Auth\UserInformation;
use App\Models\Backend\Branch;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class BranchController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $num = CoreFunction::Pagination();
        $data = Branch::where('company_id', UtilityFunction::currentCompanyId())->orderBy('id','desc')->paginate($num);
        return $this->sendListResponse($data);
    }

    public function switch_branch($branch_id){
        $current_user_id = auth()->user()->id;
        $data = UserInformation::where('sec_user_id', $current_user_id)->first();
        $data->update([
            'branch_id' => $branch_id
        ]);

        return $this->sendResponse($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'branch_name' => ['required', 'string', 'max:255'],
        ]);

        if($validator->fails()){
            return $this->sendInvalidResponse($validator->errors());
        }
        $branch = Branch::all()->last();
        $request['company_id'] = UtilityFunction::currentCompanyId();
        $request['branch_code'] = 'B-'.sprintf('%08d',!($branch) ? 1 : $branch->id + 1);

        $data = Branch::create($request->all());
        return $this->sendResponse($data);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Branch::where('company_id', UtilityFunction::currentCompanyId())->where('id', $id)->first();
        return $this->sendResponse($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'branch_name' => ['required', 'string', 'max:255'],
        ]);

        if($validator->fails()){
            return $this->sendInvalidResponse($validator->errors());
        }

        $data = Branch::where('company_id', UtilityFunction::currentCompanyId())->where('id', $id)->first();
        $data->update($request->all());
        return $this->sendResponse($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $arr_id = explode(',', $id);
        $data = "";
        foreach ($arr_id as $val) {
            $data = Branch::where('id',$val)->where('company_id', UtilityFunction::currentCompanyId())->first();
            $data -> delete();
        }
        return $this->sendResponse($data);
    }

    public function isActive(Request $request ,$id){
        $arr_id = explode(',', $id);
        $data = "";
        foreach ($arr_id as $val) {
            $data = Branch::where('id',$val)->where('company_id', UtilityFunction::currentCompanyId())->first();
            $data ->update([
                'is_active' => $request['is_active']
            ]);
        }
        return $this->sendResponse($data);
    }

}
