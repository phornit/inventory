<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Resources\CommuneCollection;
use App\Http\Resources\CommuneResource;
use App\Http\Resources\DistrictCollection;
use App\Libraries\CoreFunction;
use App\Models\Location\City;
use App\Models\Location\Commune;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CommuneController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $num = CoreFunction::Pagination();
        $data = Commune::orderBy('id','desc')->paginate($num);
        return new CommuneCollection($data);
    }

    public function getCommuneByDistrict($district_id){
        $num = CoreFunction::Pagination();
        $data = Commune::where('district_id', $district_id)->where('is_active',1)->orderBy('id','desc')->paginate($num);
        return new CommuneCollection($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title_kh' => ['required', 'string', 'max:255'],
            'title_en' => ['required', 'string', 'max:255']
        ]);

        if($validator->fails()){
            return $this->sendInvalidResponse($validator->errors());
        }

        $data = Commune::create($request->all());
        return $this->sendResponse($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Commune::find($id);
        return new CommuneResource($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'title_kh' => ['required', 'string', 'max:255'],
            'title_en' => ['required', 'string', 'max:255']
        ]);

        if($validator->fails()){
            return $this->sendInvalidResponse($validator->errors());
        }

        $data = Commune::find($id);
        $data ->update($request->all());
        return $this->sendResponse($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $arr_id = explode(',', $id);
        $data = "";
        foreach ($arr_id as $val) {
            $data = Commune::find($val);
            $data -> delete();
        }
        return $this->sendResponse($data);
    }

    public function isActive(Request $request ,$id){
        $arr_id = explode(',', $id);
        $data = "";
        foreach ($arr_id as $val) {
            $data = Commune::find($val);
            $data ->update([
                'is_active' => $request['is_active']
            ]);
        }
        return $this->sendResponse($data);
    }
}
