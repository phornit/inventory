<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Resources\ExchangeRateCollection;
use App\Http\Resources\ExchangeRateResource;
use App\Libraries\CoreFunction;
use App\Libraries\UtilityFunction;
use App\Models\Backend\ExchangeRate;
use App\Models\Backend\ExchangeRateBranch;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ExchangeRateController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Display a listing of the resource.
     *
     * @param account_id
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $num = CoreFunction::Pagination();
        $data = ExchangeRate::where('company_id', UtilityFunction::currentCompanyId())->orderBy('id','desc')->paginate($num);
        return new ExchangeRateCollection($data);
    }

    public function exChangRate($branch_id){
        $num = CoreFunction::Pagination();
        $exchangeRateBranch = ExchangeRateBranch::where('branch_id', $branch_id)->get('exchange_rate_id');
        $data = ExchangeRate::where('company_id', UtilityFunction::currentCompanyId())->whereIn('id', $exchangeRateBranch)->paginate($num);
        return new ExchangeRateCollection($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'exchange_rate' => ['required', 'integer'],
        ]);

        if($validator->fails()){
            return $this->sendInvalidResponse($validator->errors());
        }

        $request['company_id'] = UtilityFunction::currentCompanyId();
        $data = ExchangeRate::create($request->all());
        if ($request['branches']){
            foreach ($request['branches'] as $value){
                ExchangeRateBranch::create([
                    'exchange_rate_id' => $data->id,
                    'branch_id' => $value['id']
                ]);
            }
        }

        return new ExchangeRateResource($data);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = ExchangeRate::where('id', $id)->where('company_id', UtilityFunction::currentCompanyId())->first();
        return new ExchangeRateResource($data);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'exchange_rate' => ['required', 'numeric'],
        ]);

        if($validator->fails()){
            return $this->sendInvalidResponse($validator->errors());
        }

        $data = ExchangeRate::where('id', $id)->where('company_id', UtilityFunction::currentCompanyId())->first();
        $data->update($request->all());

        if ($request['branches']){
            ExchangeRateBranch::where('exchange_rate_id', $id)->delete();
            foreach ($request['branches'] as $value){
                ExchangeRateBranch::create([
                    'exchange_rate_id' => $id,
                    'branch_id' => $value['id']
                ]);
            }
        }

        return new ExchangeRateResource($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $arr_id = explode(',', $id);
        $data = "";
        foreach ($arr_id as $val) {
            ExchangeRateBranch::where('exchange_rate_id', $val)->delete();
            $data = ExchangeRate::where('id',$val)->where('company_id', UtilityFunction::currentCompanyId())->first();
            $data -> delete();
        }
        return new ExchangeRateResource($data);
    }

    public function isActive(Request $request ,$id){
        $arr_id = explode(',', $id);
        $data = "";
        foreach ($arr_id as $val) {
            $data = ExchangeRate::where('id',$val)->where('company_id', UtilityFunction::currentCompanyId())->first();
            $data ->update([
                'is_active' => $request['is_active']
            ]);
        }
        return new ExchangeRateResource($data);
    }
}
