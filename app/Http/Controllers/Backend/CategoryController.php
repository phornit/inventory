<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Resources\CategoriesCollection;
use App\Http\Resources\CategoriesResource;
use App\Libraries\CoreFunction;
use App\Libraries\UtilityFunction;
use App\Models\Backend\Category;
use App\Models\Backend\CategoryBranch;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class CategoryController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $num = CoreFunction::Pagination();
        $data = Category::where('company_id', UtilityFunction::currentCompanyId())->where('parent_id',0)->orderBy('id','desc')->paginate($num);
        return new CategoriesCollection($data);
    }

    public function getCategoryByBranch($branch_id){
        $arr_cat_id = CategoryBranch::where('branch_id', $branch_id)->get('category_id');
        $data = Category::whereIn('id', $arr_cat_id)->where('parent_id',0)->paginate(CoreFunction::Pagination());
        return new CategoriesCollection($data);
    }

    public function getAllParent(){
        $data = Category::where('company_id', UtilityFunction::currentCompanyId())->where('parent_id',0)->get();
        return new CategoriesCollection($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
        ]);

        if($validator->fails()){
            return $this->sendInvalidResponse($validator->errors());
        }
        $request['company_id'] = UtilityFunction::currentCompanyId();
        $data = Category::create($request->all());

        if ($request['branches']) {
            foreach ($request['branches'] as $value) {
                CategoryBranch::create([
                    'category_id' => $data->id,
                    'branch_id' => $value['id'],
                ]);
            }
        }

        return new CategoriesResource($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Category::find($id);
        return new CategoriesResource($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255'],
        ]);

        if($validator->fails()){
            return $this->sendInvalidResponse($validator->errors());
        }
        $request['company_id'] = UtilityFunction::currentCompanyId();
        $data = Category::find($id);
        $data->update($request->all());

        CategoryBranch::where('category_id', $id)->delete();

        if ($request['branches']) {
            foreach ($request['branches'] as $value) {
                CategoryBranch::create([
                    'category_id' => $data->id,
                    'branch_id' => $value['id'],
                ]);
            }
        }

        return new CategoriesResource($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $arr_id = explode(',', $id);
        $data = "";
        foreach ($arr_id as $val) {
            CategoryBranch::where('category_id', $val)->delete();
            $data = Category::where('id',$val)->where('company_id', UtilityFunction::currentCompanyId())->first();
            $data -> delete();
        }
        return new CategoriesResource($data);
    }

    public function isActive(Request $request ,$id){
        $arr_id = explode(',', $id);
        $data = "";
        foreach ($arr_id as $val) {
            $data = Category::where('id',$val)->where('company_id', UtilityFunction::currentCompanyId())->first();
            $data ->update([
                'is_active' => $request['is_active']
            ]);
        }
        return new CategoriesResource($data);
    }
}
