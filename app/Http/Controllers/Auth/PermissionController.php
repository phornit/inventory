<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Libraries\UtilityFunction;
use Illuminate\Http\Request;
use App\Models\Auth\UserPermission;
use App\Models\Menus\BackendMenu;

class PermissionController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth:api');
    }

    public function checkMenusPermission(Request $request){
        $menu_link = $request->menu_link;
        $currentRoleId = UtilityFunction::getCurrentUserRole();
        $menu = BackendMenu::where('link', $menu_link)->first();
        $data = UserPermission::whereIn('role_id', $currentRoleId)->where('backend_menu_id', $menu->id)->first();
        return $this->sendResponse($data);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
