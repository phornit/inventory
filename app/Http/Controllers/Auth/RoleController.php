<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Resources\RoleResource;
use App\Libraries\CoreFunction;
use App\Models\Auth\Role;
use App\Models\Auth\UserPermission;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $num = CoreFunction::Pagination();
        $data = Role::orderBy('id', 'desc')->paginate($num);
        return $this->sendListResponse($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255']
        ]);

        if($validator->fails()){
            return $this->sendInvalidResponse($validator->errors());
        }

        $data = Role::create($request->all());

        if ($data){
            foreach ($request['menus'] as $menu){
                UserPermission::create([
                    'role_id' => $data->id,
                    'backend_menu_id' => $menu['backend_menu_id'],
                    'm_view' => $menu['m_view'],
                    'm_insert' => $menu['m_insert'],
                    'm_update' => $menu['m_update'],
                    'm_delete' => $menu['m_delete']
                ]);
            }
        }
        return new RoleResource($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Role::find($id);
        return new RoleResource($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255']
        ]);

        if($validator->fails()){
            return $this->sendInvalidResponse($validator->errors());
        }

        $role = Role::find($id);
        $role->update($request->all());

        if ($request['menus']){
            foreach ($request['menus'] as $menu){
                if ($menu['id']>0){
                    $data = UserPermission::find($menu['id']);
                    $data->update([
                        'm_view' => $menu['m_view'],
                        'm_insert' => $menu['m_insert'],
                        'm_update' => $menu['m_update'],
                        'm_delete' => $menu['m_delete']
                    ]);
                }else{
                    UserPermission::create([
                        'role_id' => $id,
                        'backend_menu_id' => $menu['backend_menu_id'],
                        'm_view' => $menu['m_view'],
                        'm_insert' => $menu['m_insert'],
                        'm_update' => $menu['m_update'],
                        'm_delete' => $menu['m_delete']
                    ]);
                }
            }
        }

        return new RoleResource($role);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $arr_id = explode(',', $id);
        $data = "";
        foreach ($arr_id as $val) {
            UserPermission::where('role_id', $val)->delete();
            $data = Role::find($val);
            $data -> delete();
        }
        return $this->sendResponse($data);
    }

    public function isActive(Request $request ,$id){
        $arr_id = explode(',', $id);
        $data = "";
        foreach ($arr_id as $val) {
            $data = Role::find($val);
            $data ->update([
                'is_active' => $request['is_active']
            ]);
        }
        return $this->sendResponse($data);
    }
}
