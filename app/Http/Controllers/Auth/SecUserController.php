<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Resources\currentUserResource;
use App\Http\Resources\SecUserCollection;
use App\Http\Resources\SecUserResource;
use App\Libraries\CoreFunction;
use App\Libraries\UtilityFunction;
use App\Models\Auth\UserRole;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Cookie;

class SecUserController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $num = CoreFunction::Pagination();
        $data = User::orderBy('id', 'desc')->paginate($num);
        return new SecUserCollection($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = User::find($id);
        return new SecUserResource($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);
        $user->update([
            'name' => $request['name'],
            'email' => $request['email'],
            'is_active' => $request['is_active']
        ]);
        UserRole::where('sec_user_id',$id)->delete();
        if ($user){
            foreach ($request['user_role'] as $role){
                UserRole::create([
                    'sec_user_id' => $id,
                    'role_id' => $role['id']
                ]);
            }
        }
        return $this->sendResponse($user);
    }

    /**
     * 
     */

    public function switch_lang(){
        $lang = \Request::get('lang');
        $user_id = auth()->user()->id;
        $user = User::find($user_id);
        $user->update([
            'default_language' => $lang
        ]);
        return $this->sendResponse($user);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $arr_id = explode(',', $id);
        $data = "";
        foreach ($arr_id as $val) {
            $data = User::find($val);
            $data -> delete();
        }
        return $this->sendResponse($data);
    }

    /**
     * Get current user profile have been login
     * @param int $user_id
     * @return object users
     */
    public function getCurrentUser(){
        $data['user_id'] = UtilityFunction::getCurrentUserInfo()['id'];
        $data['name'] = UtilityFunction::getCurrentUserInfo()['name_en'];
        $data['email'] = UtilityFunction::getCurrentUser()['email'];
        $data['company_id'] = UtilityFunction::currentCompanyId();
        $data['branch_id'] = UtilityFunction::currentBranchId();
        $data['sec_user_id'] = UtilityFunction::getCurrentUser()['id'];
        $data['default_language'] = UtilityFunction::currentLanguage();
        return $this->sendResponse($data);
    }

    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        $cookie = Cookie::forget('_token');
        return response()->json([
            'access_token' => null,
            'statusCode' => 200,
            'message' => 'Success'
        ])->withCookie($cookie);
    }

    public function isActive(Request $request ,$id){
        $arr_id = explode(',', $id);
        $data = "";
        foreach ($arr_id as $val) {
            $data = User::find($val);
            $data ->update([
                'is_active' => $request['is_active']
            ]);
        }
        return $this->sendResponse($data);
    }

    public function isLoggedin(){
        if (Auth::check()) {
            return true;
        }else{
            return false;
        }
    }
}
