<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Mail\SendMailVerifyEmail;
use App\Models\Auth\UserInformation;
use App\Models\Auth\UserRole;
use App\Models\Backend\Branch;
use App\Models\Backend\Company;
use App\Models\Backend\Currency;
use App\Models\Backend\Department;
use App\Models\Backend\Group;
use App\Models\Backend\Position;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }

    protected function UserRegister(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => ['required', 'string', 'max:255', 'unique:core_sec_user'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:core_sec_user'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ]);

        if($validator->fails()){
            return $this->sendInvalidResponse($validator->errors());
        }

        $secUser = User::create([
            'name' => $request['name'],
            'email' => $request['email'],
            'password' => Hash::make($request['password']),
            'is_active' => $request['is_active'],
            'expired_date' => null,
            'default_language' => 'en'
        ]);

        if ($secUser){

            $currency = Currency::first();

            $company = Company::create([
                'name_en' => $request['name'],
                'city_id' => 1
            ]);

            $branch = Branch::create([
                'company_id' => $company->id,
                'branch_code' => '12345',
                'branch_name' => 'Default Branch',
                'currency_id' => $currency->id
            ]);

            $department = Department::create([
                'company_id' => $company->id,
                'title_kh' => 'ផ្នែក',
                'title_en' => 'Default Department',
                'is_active' => true
            ]);   
            
            $group = Group::create([
                'company_id' => $company->id,
                'title_kh' => 'ក្រុម',
                'title_en' => 'Default Group',
                'is_active' => true
            ]);   

            $position = Position::create([
                'company_id' => $company->id,
                'title_kh' => 'តួនាទី',
                'title_en' => 'Default Position',
                'is_active' => true
            ]);   

            $userInfo = UserInformation::create([
                'name_en' => $request['name'],
                'sec_user_id' => $secUser->id,
                'company_id' => $company->id,
                'branch_id' => $branch->id,
                'department_id' => $department->id,
                'group_id' => $group->id,
                'position_id' => $position->id
            ]);

            foreach ($request['user_role'] as $role){
                UserRole::create([
                    'sec_user_id' => $secUser->id,
                    'role_id' => $role['id']
                ]);
            }
        }

        return $this->sendResponse($secUser);

//        if ($result){
//            $id = Crypt::encrypt($result->id);
//            $data['name'] = $request['name'];
//            $data['id'] = $id;
//
//            Mail::to($request['email'])->queue(new SendMailVerifyEmail($result));
//
//
//            return Redirect::to('/register')
//                ->with([
//                    'message' => '<div class="reg_sms_success">ការចុឈ្មោះរបស់អ្នកទទួលបានជោគជ័យ! សូមចូលទៅកាន់អ៊ីមែលរបស់អ្នកដើម្បីដំណើរការគណនីរបស់អ្នក។</div>',
//                ]);
//        }
    }
}
