<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AdminController extends Controller
{
    public function index()
    {
        return view('admin');
    }

    public function login()
    {
        return view('auth');
    }

    public function admin()
    {
        return view('admin');
    }

    public function register()
    {
        return view('auth');
    }
}
