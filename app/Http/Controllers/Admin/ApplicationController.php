<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Resources\BackendMenuCollection;
use App\Http\Resources\MenuNoParentCollection;
use App\Libraries\CoreFunction;
use App\Models\Admin\Application;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ApplicationController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $num = CoreFunction::Pagination();
        $data = Application ::where('parent_id',0)->orderBy('ordering', 'desc')->paginate($num);

        return new BackendMenuCollection($data);
    }


    /**
     * Display a list of the application
     * @return all application is active and order by desc
     */

    public function getAll()
    {
        $data = Application::where('is_active',1)->orderBy('ordering', 'desc')->get();
        return $this->sendListResponse($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'title_en' => ['required', 'string', 'max:255'],
            'title_kh' => ['required', 'string', 'max:255']
        ]);

        $request['is_active'] = (!empty($request['is_active'])) ? 1 : 0;

        if($validator->fails()){
            return $this->sendInvalidResponse($validator->errors());
        }

        $data = Application::create($request->all());
        return $this->sendResponse($data);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = Application::find($id);
        return $this->sendResponse($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'title_kh' => ['required', 'string', 'max:255'],
            'title_en' => ['required', 'string', 'max:255']
        ]);

        if($validator->fails()){
            return $this->sendInvalidResponse($validator->errors());
        }

        $request['is_active'] = (!empty($request['is_active'])) ? 1 : 0;

        $data = Application::find($id);
        $data->update($request->all());
        return $this->sendResponse($data);
    }

    public function getAllParent()
    {
        $data = Application::where('parent_id',0)->where('is_active',1)->orderBy('ordering', 'desc')->get();
        return new BackendMenuCollection($data);
    }

    public function getNoParent(){
        $menu = Application::groupBy('parent_id')->where('parent_id','>', 0)->get('parent_id');
        $data = Application::whereNotIn('id', $menu)->get();
        return new MenuNoParentCollection($data);
    }

    public function getAllMenus(){
        $data = Application::all();
        return new MenuNoParentCollection($data);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data = Application::find($id);
        $data->delete();
        return $this->sendResponse($data);
    }

    public function updateOrdering(Request $request){
        $total = $request['total'];
        $menus = $request['menus'];
        foreach ($menus as $menu){
            $id = $menu['id'];
            $backendMenu = Application::find($id);
            $backendMenu->update([
                'ordering' => $total
            ]);
            $total = $total-1 ;
        }
        return $this->sendResponse($menus);
    }
}
