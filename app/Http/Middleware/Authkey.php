<?php

namespace App\Http\Middleware;

use App\Enums\EnumsConfig;
use App\Http\Controllers\Controller;
use Closure;

class Authkey extends Controller
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $key = EnumsConfig::XAPIKEY;
        $token = $request->header('X-API-KEY');
        if ($token != $key){
            return $this->sendInvalidApiKeyResponse();
        }
        return $next($request);

    }
}
