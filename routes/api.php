<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::middleware('auth:api')->get('/logout', function (Request $request) {
    return $request->user();
});

//Route::group(['middleware' => 'auth.api'], function() {
//    Route::get('logout', 'Auth\SecUserController@logout');
//});

//Auth
Route::post('authenticate', 'Auth\LoginController@authenticate');
Route::get('getCurrentUser', 'Auth\SecUserController@getCurrentUser');
Route::put('switch_lang', 'Auth\SecUserController@switch_lang');
Route::get('logout', 'Auth\SecUserController@logout');
Route::get('isloggedin', 'Auth\SecUserController@isLoggedin');
Route::post('userRegister', 'Auth\RegisterController@UserRegister');
Route::put('switch_branch/{branch_id}', 'Backend\BranchController@switch_branch');
Route::put('backendMenu-update-ordering', 'Backend\BackendMenuController@updateOrdering');
Route::get('getAllMenus', 'Backend\BackendMenuController@getAllMenus');
Route::get('checkMenusPermission','Auth\PermissionController@checkMenusPermission');
Route::get('backendMenu-get-all-parent','Backend\BackendMenuController@getAllParent');


$arr_auth = [
    'role' => 'RoleController',
    'secUsers' => 'SecUserController'
];

foreach ($arr_auth as $key => $value){
    Route::apiResources([$key => 'Auth\\'.$value]);
    Route::put($key.'-update-ordering', 'Auth\\'.$value.'@updateOrdering');
    Route::get($key.'-get-all', 'Auth\\'.$value.'@getAll');
    Route::get($key.'-get-no-parent', 'Auth\\'.$value.'@getNoParent');
    Route::put($key.'/isActive/{id}', 'Auth\\'.$value.'@isActive');
    Route::get($key.'/trash/', 'Auth\\'.$value.'@trash');
    Route::get($key.'/trash/restoreselected/{ids}', 'Auth\\'.$value.'@restoreselected');
    Route::get($key.'/trash/deleteselected/{ids}/', 'Auth\\'.$value.'@deleteselected');
}

$arr_backend =	[
    'application' => 'ApplicationController',
    'company' => 'CompanyController',
    'branch' => 'BranchController',
    'backendMenu'	=> 'BackendMenuController',
    'countries' => 'CountryController',
    'city'	=> 'CityController',
    'district'	=> 'DistrictController',
    'commune'	=> 'CommuneController',
    'village'	=> 'VillageController',
    'currency'	=> 'CurrencyController',
    'exchangeRate'	=> 'ExchangeRateController',
    'categories' => 'CategoryController',
    'warehouse' => 'WarehouseController',
    'department' => 'DepartmentController',
    'group' => 'GroupController',
    'position' => 'PositionController',
];

foreach ($arr_backend as $key => $value)
{
    Route::apiResources([$key => 'Backend\\'.$value]);
    Route::put($key.'-update-ordering', 'Backend\\'.$value.'@updateOrdering');
    Route::get($key.'-get-all', 'Backend\\'.$value.'@getAll');
    Route::get($key.'-get-no-parent', 'Backend\\'.$value.'@getNoParent');
    Route::put($key.'/isActive/{id}', 'Backend\\'.$value.'@isActive');
    Route::get($key.'/trash/', 'Backend\\'.$value.'@trash');
    Route::get($key.'/trash/restoreselected/{ids}', 'Backend\\'.$value.'@restoreselected');
    Route::get($key.'/trash/deleteselected/{ids}/', 'Backend\\'.$value.'@deleteselected');
}

//CoreBackEnd
Route::post('fileUpload', 'Backend\FileUploadController@fileUpload');
Route::post('deleteImg', 'Backend\FileUploadController@removeImage');
Route::post('deleteFile', 'Backend\FileUploadController@removeFile');
Route::post('resizeImagePost', 'Backend\FileUploadController@resizeImagePost');
Route::post('imagePost', 'Backend\FileUploadController@imagePost');

Route::get('get-current-company', 'Backend\CompanyController@getCurrentCompany');
Route::get('exChangRateByBranchId/{branch_id}', 'Backend\ExchangeRateController@exChangRate');
Route::get('getCategoryByBranch/{branch_id}', 'Backend\CategoryController@getCategoryByBranch');

Route::get('getDistrictByCity/{id}', 'Backend\DistrictController@getDistrictByCity');
Route::get('getCommuneByDistrict/{id}', 'Backend\CommuneController@getCommuneByDistrict');
Route::get('getVillageByCommune/{id}', 'Backend\VillageController@getVillageByCommune');
