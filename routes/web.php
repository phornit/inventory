<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Auth::routes();
Route::get('/', 'AdminController@login');
Route::get('/register', 'AdminController@register');
Route::get('/admin', 'AdminController@admin')->name('admin');

$arr_admin = [
    'dashboard',
    'application',
    'backend-menus',
    'frontend-menus',
    'roles',
    'user-account',
    'countries',
    'city',
    'district',
    'commune',
    'village',
    'currency',
    'company',
    'branch',
    'products',
    'exchange-rate',
    'category',
    'warehouse',
    'department',
    'group',
    'position',
    'employee'
];
foreach ($arr_admin as $menu) {
    Route::get('/admin/'.$menu, 'AdminController@admin')->name('admin');
}
