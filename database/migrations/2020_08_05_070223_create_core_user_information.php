<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCoreUserInformation extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('core_user_information', function (Blueprint $table) {
            $table->id();
            $table->string('name_kh')->nullable();
            $table->string('name_en')->nullable();
            $table->string('gender')->nullable();
            $table->dateTime('date_of_birth')->nullable();
            $table->string('place_of_birth')->nullable();
            $table->string('phone')->nullable();
            $table->string('address')->nullable();
            $table->integer('nationality')->nullable();
            $table->integer('department_id')->nullable();
            $table->integer('group_id')->nullable();
            $table->integer('position_id')->nullable();
            $table->string('skill')->nullable();
            $table->string('degree')->nullable();
            $table->string('photo')->nullable();
            $table->integer('sec_user_id')->nullable();
            $table->integer('company_id')->nullable();
            $table->integer('branch_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('core_user_information');
    }
}
