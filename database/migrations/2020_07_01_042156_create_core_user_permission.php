<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCoreUserPermission extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('core_user_permission', function (Blueprint $table) {
            $table->id();
            $table->integer('role_id');
            $table->integer('backend_menu_id');
            $table->boolean('m_view');
            $table->boolean('m_insert');
            $table->boolean('m_update');
            $table->boolean('m_delete');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('core_user_permission');
    }
}
