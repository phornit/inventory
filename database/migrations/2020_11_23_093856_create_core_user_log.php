<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCoreUserLog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('core_user_log', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('user_id');
            $table->string('ip_address');
            $table->string('browser_info');
            $table->timestamp('login_date');
            $table->timestamp('logout_date')->nullable(true);
            $table->boolean('is_login');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('core_user_log');
    }
}
