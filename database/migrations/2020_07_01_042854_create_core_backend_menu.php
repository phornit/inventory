<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCoreBackendMenu extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('core_backend_menu', function (Blueprint $table) {
            $table->id();
            $table->integer('parent_id');
            $table->string('title_kh');
            $table->string('title_en');
            $table->string('link')->nullable();
            $table->string('class')->nullable();
            $table->boolean('state');
            $table->string('color')->nullable();
            $table->integer('ordering');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('core_backend_menu');
    }
}
