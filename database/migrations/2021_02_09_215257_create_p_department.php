<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePDepartment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('p_department', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('company_id');
            $table->string('title_kh');
            $table->string('title_en');
            $table->string('description')->nullable(true);
            $table->tinyInteger('is_active')->default(true);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('p_department');
    }
}
