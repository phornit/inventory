<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLocCountry extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('loc_country', function (Blueprint $table) {
            $table->id();
            $table->integer('post_code')->nullable(true);
            $table->string('alpha_2_code')->nullable(true);
            $table->string('alpha_3_code')->nullable(true);
            $table->string('title');
            $table->string('nationality');
            $table->boolean('is_active');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('loc_country');
    }
}
