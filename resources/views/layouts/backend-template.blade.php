
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Cam Market | Content Management System</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="/css/app.css">
    <link rel="stylesheet" href="/css/icheck.css">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
{{--    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/icheck-bootstrap@3.0.1/icheck-bootstrap.min.css" />--}}
    <style>
        html, body {
            height: 100%;
            margin: 0;
        }
        .text-sm {
            font-size: 0.805rem !important;
        }
        .table th, .table td {
            padding: 0.35rem;
            vertical-align: middle;
            border-top: 1px solid rgba(223, 227, 231, 0.45);
        }
        .text-sm .btn {
            font-size: 0.775rem !important;
        }
        .text-sm .btn-xs {
            padding: 0.45rem 0.4rem;
            font-size: 0.675rem !important;
            line-height: 0.5;
            border-radius: 0.2rem;
        }

        .text-sm .dropdown-item{
            font-size: 0.675rem !important;
        }
        .btn-group, .btn-group-vertical {
            position: relative;
            display: inline-flex;
            vertical-align: middle;
            right: -20px;
        }
        .form-control {
            font-size: 0.8rem;
        }
        .input-group-sm > .form-control:not(textarea), .input-group-sm > .custom-select {
            height: calc(1.8em + 0.5rem + 2px);
        }
        
        .cus-select{
            font-size: 0.805rem !important;
            margin-top: 3px !important;
            border: 0px !important;
            border-bottom: 1px solid #dee2e6 !important;
            padding: 0px !important;
            margin-right: 15px !important;
            padding-top: 0px !important;
            padding-bottom: 0px !important;
        }

        .text-sm .content-header h1 {
            font-size: 1.1rem;
        }

        a.tmenusel::after, a.tmenu:hover::after {
            content: "";
            position: absolute;
            bottom: 0px;
            left: 50%;
            left: calc(50% - 6px);
            width: 0;
            height: 0;
            border-style: solid;
            border-width: 0px 10px 9px 10px;
            border-color: transparent transparent #ffffff transparent;
            top: 45px;
        }

        .layout-navbar-fixed .wrapper .main-header {
            left: 0;
            position: fixed;
            right: 0;
            top: 0;
            z-index: 1037;
            height: 60px;
        }
    </style>
</head>
<body class="hold-transition sidebar-mini text-sm layout-fixed layout-navbar-fixed">
<!-- Site wrapper -->
<div class="wrapper" id="app">
    <router-view></router-view>
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="/js/app.js"></script>
</body>
</html>
