/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

import i18n from './i18n';
import Store from './Store';
window.Fire = new Vue();

axios.defaults.baseURL = 'http://127.0.0.1:8000/';

axios.defaults.headers.common['Content-Type'] = 'application/json';
axios.defaults.headers.common['accept'] = 'application/json';
axios.defaults.headers.common['X-API-KEY'] = '1ccbc4c913bc4ce785a0a2de444aa0d6';

let access_token = Store.getters.getAccessToken;

if (access_token)
    axios.defaults.headers.common['Authorization'] = `Bearer ${access_token}`;

window.Store = Store;

Vue.prototype.$accessToken = Store.getters.getAccessToken;
Vue.prototype.$currentUser = Store.getters.getCurrentUser;

//----------------------Multi Language----------------------------------
import VueI18n from 'vue-i18n';
Vue.use(VueI18n);

//------------------------------ Flag ----------------------------------
import FlagIcon from 'vue-flag-icon';
Vue.use(FlagIcon);

//--------------------------Popup Form-----------------------------------
import swal from 'sweetalert2';
window.swal = swal;

//------------ screen size ---------------------------------------------
import VueWindowSize from 'vue-window-size';
Vue.use(VueWindowSize);

//--------------------- Form -------------------------------------------
import { Form, HasError, AlertError } from 'vform';
window.Form = Form;
Vue.component(HasError.name, HasError);
Vue.component(AlertError.name, AlertError);

//------------------------ Toastr ---------------------------------------
import 'cxlt-vue2-toastr/dist/css/cxlt-vue2-toastr.css';
import CxltToastr from 'cxlt-vue2-toastr';

var toastrConfigs = {
    position: 'top right',
    showDuration: 20
};
Vue.use(CxltToastr, toastrConfigs);

//--------------------------Date time format------------------------------
import moment from 'moment';
Vue.filter('myDate', function(created) {
    return moment(created).format('MMMM Do YYYY');
});

Vue.filter('myDOB', function(created) {
    return moment(created).format('DD-MM-YYYY');
});

Vue.filter('dateCreated', function(created) {
    return moment(created).format('DD-MM-YYYY hh:mm:ss');
});

//----------------- v-select ----------------------------------
import vSelect from 'vue-select'
Vue.component('v-select', vSelect)

//----------------- Load Pages --------------------------------
import VueLoading from 'vuejs-loading-plugin'

// using default options
Vue.use(VueLoading);

// overwrite defaults
Vue.use(VueLoading, {
    dark: true, // default false
    text: 'Ladataan', // default 'Loading'
    loading: true, // default false
    //   customLoader: myVueComponent, // replaces the spinner and text with your own
    background: 'rgb(255,255,255)', // set custom background
    classes: ['myclass'] // array, object or string
});


/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))

Vue.component('example-component', require('./components/ExampleComponent.vue').default);

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component(
    'passport-clients',
    require('./components/passport/Clients.vue').default
);

Vue.component(
    'passport-authorized-clients',
    require('./components/passport/AuthorizedClients.vue').default
);

Vue.component(
    'passport-personal-access-tokens',
    require('./components/passport/PersonalAccessTokens.vue').default
);

// vue router
import VueRouter from 'vue-router';
Vue.use(VueRouter);

import { routes } from './routes';
const router = new VueRouter({
    routes, // short for `routes: routes`
    mode: 'history',
});

const app = new Vue({
    el: '#app',
    i18n,
    router,
});