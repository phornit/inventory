import BackendTemplate from "./components/layouts/backend-template"
import Dashboard from "./components/dashboard"
import Login from "./components/login"
import Register from "./components/register"
import Home from "./components/home"
import BackendMenus from "./components/menus/backend-menus"
import FrontendMenus from "./components/menus/frontend-menus"
import Role from "./components/auth/roles"
import SecUser from "./components/auth/sec-users"
import Applcation from "./components/admin/application"

//================ Setup =========================
import Currency from "./components/setup/currency"
import ExchangeRate from "./components/setup/exchangeRate"
import Warehouse from "./components/setup/warehouse"

//================ Company =======================
import Company from "./components/backend/company"
import Branch from "./components/backend/branch"

//================ Product =======================
import Product from "./components/backend/Product/product"
import Category from "./components/backend/Product/category"

//================= Location ======================
import Country from "./components/location/countries"
import City from "./components/location/city"
import District from "./components/location/district"
import Commune from "./components/location/commune"
import Village from "./components/location/village"

//================ Person ========================
import Department from "./components/person/Department"
import Group from "./components/person/group"
import Position from "./components/person/position"
import Employee from "./components/person/employee"

export const routes = [{
        name: 'Login',
        path: '/',
        component: Login,
    },
    {
        name: 'Register',
        path: '/register',
        component: Register,
    },
    {
        name: 'BackendTemplate',
        path: '/admin',
        component: BackendTemplate,
        children: [
            { path: '/home', name: 'Home', component: Home },
            { path: '/admin/dashboard', name: 'Dashboard', component: Dashboard },
            { path: '/admin/application', name: 'Applcation', component: Applcation },
            { path: '/admin/roles', name: 'Role', component: Role },
            { path: '/admin/user-account', name: 'SecUser', component: SecUser },
            { path: '/admin/backend-menus', name: 'BackendMenus', component: BackendMenus },
            { path: '/admin/frontend-menus', name: 'FrontendMenus', component: FrontendMenus },

            //================ Product ================================
            { path: '/admin/products', name: 'Product', component: Product },
            { path: '/admin/category', name: 'Category', component: Category },

            //================= Setup =================================
            { path: '/admin/currency', name: 'Currency', component: Currency },
            { path: '/admin/exchange-rate', name: 'ExchangeRate', component: ExchangeRate },
            { path: '/admin/warehouse', name: 'Warehouse', component: Warehouse },

            //================= Company ===============================
            { path: '/admin/company', name: 'Company', component: Company },
            { path: '/admin/branch', name: 'Branch', component: Branch },

            //================= Location ==============================
            { path: '/admin/countries', name: 'Countries', component: Country },
            { path: '/admin/city', name: 'City', component: City },
            { path: '/admin/district', name: 'District', component: District },
            { path: '/admin/commune', name: 'Commune', component: Commune },
            { path: '/admin/village', name: 'Village', component: Village },

            //================= Person ============================
            { path: '/admin/department', name: 'Department', component: Department },
            { path: '/admin/group', name: 'Group', component: Group },
            { path: '/admin/position', name: 'Postion', component: Position },
            { path: '/admin/employee', name: 'Employee', component: Employee },
        ]
    }
];