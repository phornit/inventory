import Vue from 'vue';
import VueI18n from 'vue-i18n';

Vue.use(VueI18n);

const messages = {
    'en': {
        home: 'Home',

        menus_list: 'Menus List',
        menus_setting: 'Menus Setting',
        menu_name: 'Menu',
        menu_parent: 'Parent',
        menu_link: 'link',
        menu_class: 'Class',
        menu_color: 'Color',
        menu_ordering: 'Ordering',

        menu_msg_add_title: 'Create Menu',
        menu_msg_add_des: 'Menu have been created successful!',
        menu_msg_update_title: 'Update Menu',
        menu_msg_update_des: 'Menu have been updated successful.',

        //----------- Applicaiton --------------
        application_list: 'Application List',
        add_application: 'Add Application',
        edit_applicaiton: 'Edit Application',

        //----------- Roles --------------------
        role_name: 'Role Name',
        role_list: 'Role List',
        role_setting: 'Role Setting',

        //------------ Currency --------------------
        currency_list: 'Currency List',
        currency_setting: 'Currency Setting',
        currency_name: 'Currency Name',
        currency_abbr: 'Abbreviation',
        currency_symbol: 'Symbol',

        //------------ Exchange Rate -----------------
        exchange_rate_list: 'Exchange Rate List',
        exchange_rate_setting: 'Exchange Rate Setting',
        exchange_rate_name: 'Exchange Rate',
        from_currency: 'From Currency',
        to_currency: 'To Currency',

        //----------- Sec User -----------------
        sec_user_list: 'User Security List',
        sec_user_setting: 'User Security Setting',
        sec_user_name: 'User Name',
        sec_email: 'Email',
        sec_type: 'Type',
        password: 'Password',
        confirm_password: 'Confirm Password',

        //----------- Location ------------------
        nationality: 'Nationality',
        country_list: 'Countries List',
        country_setting: 'Countries Setting',
        country: 'Country',
        city_list: 'City/Province List',
        city_setting: 'City/Province Setting',
        district_list: 'District/Khan List',
        district_setting: 'District/Khan Setting',
        commune_list: 'Commune/Sangkat List',
        commune_setting: 'Commune/Sangkat Setting',
        village_list: 'Village/Group List',
        village_setting: 'Village/Group Setting',
        post_code: 'Post Code',
        city_name: 'City/Province',
        district_name: 'District/Khan',
        commune_name: 'Commune/Sangkat',
        village_name: 'Village/Group',

        //------------- Company -----------------
        company_profile: 'Company Profile',
        update_company: 'Update Company',
        sub_name: 'Sub Name',

        //------------- Branch ------------------
        branch_setting: 'Branch Setting',
        branch_list: 'Branch List',
        branch: 'Branches',
        branch_code: 'Branch Code',
        branch_name: 'Branch Name',
        branch_phone: 'Phone',
        branch_address: 'Address',

        //------------- Category ----------------
        category: 'Category',
        categories: 'Categories',

        //------------- warehouse ---------------
        warehouse: 'Warehouse',
        warehouse_setting: 'Warehouse Setting',
        warehouse_list: 'Warehouse List',

        //-------------- Department --------------
        department_list: 'Department List',
        add_department: 'Add Department',
        edit_department: 'Edit Department',

        //-------------- Group ------------------
        group_list: 'Group List',
        add_group: 'Add Group',
        edit_group: 'Edit Group',

        //-------------- Group ------------------
        position_list: 'Position List',
        add_position: 'Add Position',
        edit_position: 'Edit Position',

        //----------- General -------------------
        name_kh: 'Name Khmer',
        name_en: 'Name English',
        phone: 'Phone',
        email: 'Email',
        facebook: 'Facebook',
        website: 'Website',
        address: 'Address',
        name: 'Name',

        title_kh: 'Title (KH)',
        title_en: 'Title (EN)',

        t_view: 'Views',
        t_insert: 'Insert',
        t_update: 'Update',
        t_delete: 'Delete',

        code: 'Code',
        setting: 'Setting',
        thumbnail: 'Thumbnail',
        parent: 'Parent',
        no_parent: 'No Parent',
        list: 'List',
        filter: 'Filters',
        save: 'Save',
        update: 'Update',
        edit: 'Edit',
        delete: 'Delete',
        add_new: 'Create',
        cancel: 'Cancel',
        search: 'Search',
        select: '---Select---',
        state: 'State',
        action: 'Action',
        desc: 'Description',
        no: 'Nº',
        p_row: 'Rows per page:',
        p_page: 'Pages',
        p_of: 'Of',
        p_total: 'Total Items',
        publish: 'Publish',
        unpublish: 'Unpublish',
    },
    'kh': {
        home: 'ទំព័រដើម',
        menus_list: 'តារាងមីនុយ',
        menus_setting: 'ការកំណត់មីនុយ',
        menu_name: 'មីនុយ',
        menu_parent: 'មីនុយមេ',
        menu_link: 'តំណរភ្ជាប់',
        menu_class: 'ស្តាយ',
        menu_color: 'ពណ៌',
        menu_ordering: 'លេខរៀង',

        //----------- Applicaiton --------------
        application_list: 'តារាងកម្មវិធី',
        add_application: 'បន្ថែមកម្មវិធី',
        edit_applicaiton: 'កែប្រែកម្មវិធី',

        //----------- Roles --------------------
        role_name: 'ឈ្មោះតួនាទី',
        role_list: 'បញ្ជីតួនាទី',
        role_setting: 'ការកំណត់តួនាទី',

        //------------ Currency --------------------
        currency_list: 'បញ្ជីរូបិយប័ណ្ណ',
        currency_setting: 'ការកំណត់រូបិយប័ណ្ណ',
        currency_name: 'ឈ្មោះរូបិយប័ណ្ណ',
        currency_abbr: 'អក្សរកាត់រូបិយប័ណ្ណ',
        currency_symbol: 'និមិត្តសញ្ញា',

        //------------ Exchange Rate -----------------
        exchange_rate_list: 'បញ្ជីអត្រាប្តូរប្រាក់',
        exchange_rate_setting: 'ការកំណត់អត្រាប្តូរប្រាក់',
        exchange_rate_name: 'អត្រាប្តូរប្រាក់',
        from_currency: 'ពីរូបិយប័ណ្ណ',
        to_currency: 'ទៅរូបិយប័ណ្ណ',

        //----------- Sec User -----------------
        sec_user_list: 'តារាងសុវត្ថិភាពអ្នកប្រើប្រាស់',
        sec_user_setting: 'កំណត់សុវត្ថិភាពអ្នកប្រើប្រាស់',
        sec_user_name: 'គណនី',
        sec_email: 'អ៊ីម៉ែល',
        sec_type: 'ប្រភេទ',
        password: 'ពាក្យសម្ងាត់',
        confirm_password: 'បញ្ជាក់ពាក្យសម្ងាត់',

        //----------- Location ------------------
        nationality: 'សញ្ជាតិ',
        country_list: 'តារាងប្រទេស',
        country: 'ប្រទេស',
        country_setting: 'ការកំណត់ប្រទេស',
        city_list: 'តារាងខេត្ត/ក្រុង',
        city_setting: 'ការកំណត់ខេត្ត/ក្រុង',
        district_list: 'តារាងស្រុក/ខណ្ខ',
        district_setting: 'ការកំណត់ស្រុក/ខណ្ខ',
        commune_list: 'តារាងឃុំ/សង្កាត់',
        commune_setting: 'ការកំណត់ឃុំ/សង្កាត់',
        village_list: 'តារាងក្រុម/ភូមិ',
        village_setting: 'ការកំណត់ក្រុម/ភូមិ',
        post_code: 'លេខកូដ',
        title_kh: 'ឈ្មោះខ្មែរ',
        title_en: 'ឈ្មោះអង់គ្លេស',
        city_name: 'ខេត្ត/ក្រុង',
        district_name: 'ស្រុក/ខណ្ខ',
        commune_name: 'ឃុំ/សង្កាត់',
        village_name: 'ក្រុម/ភូមិ',

        //------------- Company -----------------
        company_profile: 'ប្រវត្តិ​ក្រុមហ៊ុន',
        update_company: 'កែប្រែក្រុមហ៊ុន',
        sub_name: 'ឈ្មោះរង',

        //------------- warehouse ---------------
        warehouse: 'ឃ្លាំងទំនិញ',
        warehouse_list: 'តារាងឃ្លាំងទំនិញ',
        warehouse_setting: 'ការកំណត់ឃ្លាំងទំនិញ',

        //------------- Branch ------------------
        branch_setting: 'ការកំណត់សាខា',
        branch_list: 'តារាងសាខា',
        branch: 'សាខា',
        branch_name: 'ឈ្មោះសាខា',
        branch_code: 'លេខកូដសាខា',
        branch_phone: 'លេខទូរស័ព្ទ',
        branch_address: 'អាសយដ្ឋាន',

        //-------------- Department --------------
        department_list: 'តារាងផ្នែក',
        add_department: 'បន្ថែមផ្នែក',
        edit_department: 'កែប្រែផ្នែក',

        //-------------- Group ------------------
        group_list: 'តារាងក្រុម',
        add_group: 'បន្ថែមក្រុម',
        edit_group: 'កែប្រែក្រុម',

        //-------------- Group ------------------
        position_list: 'តារាងតួនាទី',
        add_position: 'បន្ថែមតួនាទី',
        edit_position: 'កែប្រែតួនាទី',

        //----------- General -------------------
        name_kh: 'ឈ្មោះខ្មែរ',
        name_en: 'ឈ្មោះអង់គ្លេស',
        phone: 'ទូរស័ព្ទ',
        email: 'អ៊ីម៉ែល',
        facebook: 'ហ្វេសប៊ុក',
        website: 'វ៉ិបសាយ',
        address: 'អសយដ្ឋាន',
        name: 'ឈ្មោះ',
        desc: 'បរិយាយ',
        t_view: 'មើល',
        t_insert: 'បញ្ចូលថ្មី',
        t_update: 'កែប្រែ',
        t_delete: 'លុប',

        code: 'លេខកូដ',
        setting: 'ការកំណត់',
        thumbnail: 'រូបភាពតូច',
        parent: 'មេ',
        list: 'តារាង',
        filter: 'បង្ហាញទិន្ន័យតាមរយៈ',
        save: 'រក្សាទុក',
        update: 'កែប្រែ',
        edit: 'កែប្រែ',
        delete: 'លុប',
        add_new: 'បង្កើតថ្មី',
        cancel: 'បោះបង់',
        search: 'ស្វែងរក',
        select: '---ជ្រើសរើស---',
        state: 'ស្ថានភាព',
        action: 'សកម្មភាព',
        no: 'ល.រ',
        p_row: 'ចំនួនទិន្នន័យ ក្នុងមួយទំព័រ',
        p_page: 'ទំព័រទី',
        p_of: 'នៃ',
        p_total: 'សរុបទិន្នន័យ',
        publish: 'ផ្សព្វផ្សាយ',
        unpublish: 'មិនផ្សព្វផ្សាយ',
    }
};

const i18n = new VueI18n({
    locale: 'en', // set locale
    fallbackLocale: 'en', // set fallback locale
    messages, // set locale messages
});

export default i18n;